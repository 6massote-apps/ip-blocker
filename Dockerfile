FROM maven:3.5.2-jdk-8-alpine

COPY . /app
WORKDIR /app

RUN mvn clean package -Dspring.profiles.active=test

CMD ["cat", "target/ipblocker-app.jar"]
