--  liquibase formatted sql
--  Changeset ipblocked-11-30-17-01::6massote
ALTER TABLE IP_STATUS
  ADD COLUMN JOB BIGINT NOT NULL;
--  rollback ALTER TABLE IP_STATUS DROP COLUMN JOB_ID;;
