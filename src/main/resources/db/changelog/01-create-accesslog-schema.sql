--  liquibase formatted sql
--  Changeset accesslog-11-26-17-01::6massote
CREATE TABLE ACCESS_LOG (
  ID          INT AUTO_INCREMENT NOT NULL,
  TIME        DATETIME(3)        NOT NULL,
  IP          VARCHAR(32)        NOT NULL,
  DESCRIPTION VARCHAR(1024)      NOT NULL,
  STATUS_CODE VARCHAR(6)         NOT NULL,
  USER_AGENT  VARCHAR(512)       NOT NULL,
  CONSTRAINT PK_ACCESS_LOG PRIMARY KEY (ID)
);
--  rollback drop table ACCESS_LOG;
