--  liquibase formatted sql
--  Changeset ipblocked-11-26-17-01::6massote
CREATE TABLE IP_STATUS (
  ID               INT AUTO_INCREMENT NOT NULL,
  IP               VARCHAR(32)        NOT NULL,
  THRESHOLD        INT                NOT NULL,
  DESCRIPTION      VARCHAR(1024)      NOT NULL,
  BLOCKED_AT       DATETIME           NOT NULL,
  BLOCKED_UNTIL_AT DATETIME           NOT NULL,
  CONSTRAINT PK_IP_STATUS PRIMARY KEY (ID)
);
--  rollback drop table IP_STATUS;