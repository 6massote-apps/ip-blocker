--  liquibase formatted sql
--  Changeset accesslog-11-30-17-01::6massote
ALTER TABLE ACCESS_LOG
  ADD COLUMN JOB BIGINT NOT NULL;
--  rollback ALTER TABLE ACCESS_LOG DROP COLUMN JOB_ID;;
