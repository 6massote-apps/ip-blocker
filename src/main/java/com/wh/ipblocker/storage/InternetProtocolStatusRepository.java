package com.wh.ipblocker.storage;

import com.wh.ipblocker.domain.model.InternetProtocolThreshold;
import com.wh.ipblocker.domain.model.InternetProtocolStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface InternetProtocolStatusRepository extends JpaRepository<InternetProtocolStatus, Long> {

    /**
     * Finds IPs that made more than a certain number of requests for a given time period.
     *
     * @param job
     * @param startTime
     * @param endTime
     * @param threshold
     * @return  A list of IPs with a certain number of requests for a given time periods.
     */
    @Query(value="SELECT new com.wh.ipblocker.domain.model.InternetProtocolThreshold(al.ip, COUNT(al.ip)) " +
            "FROM AccessLog al " +
            "WHERE al.time >= :startTime AND al.time <= :endTime " +
            "AND al.job = :job " +
            "GROUP BY al.ip " +
            "HAVING COUNT(al.ip) >= :threshold " +
            "ORDER BY COUNT(al.ip) DESC")
    List<InternetProtocolThreshold> findIpsByThresholdAndByDate(
            @Param("job") Long job,
            @Param("startTime") LocalDateTime startTime,
            @Param("endTime") LocalDateTime endTime,
            @Param("threshold") Long threshold);
}
