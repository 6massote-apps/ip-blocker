package com.wh.ipblocker;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

/**
 * Application runs using ApplicationRunner of Springboot Framework.
 * Once the Springboot application context is up, spring will handle following runner:
 * 1) @class {@link IpBlockerRunner}
 *
 * @author 6massote
 */
@EntityScan(
        basePackageClasses = {IpblockerApplication.class, Jsr310JpaConverters.class}
)
@SpringBootApplication
public class IpblockerApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder()
                .sources(IpblockerApplication.  class)
                .bannerMode(Banner.Mode.OFF)
                .run(args);
    }

}
