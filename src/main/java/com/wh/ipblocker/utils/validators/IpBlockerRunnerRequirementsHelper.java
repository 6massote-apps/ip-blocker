package com.wh.ipblocker.utils.validators;

import com.wh.ipblocker.domain.model.Duration;
import com.wh.ipblocker.domain.model.IpBlockerRunnerRequirements;
import lombok.extern.java.Log;
import org.springframework.boot.ApplicationArguments;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Log
public class IpBlockerRunnerRequirementsHelper {

    /**
     *  Convert args to a specific Map
     *
      * @param args
     * @return Map of arguments
     */
    public static IpBlockerRunnerRequirements convert(final ApplicationArguments args) {
        if (args.getOptionNames().size() == 0) {
            log.warning(String.format("IpBlockerRunnerRequirements require at least one argument."));

            return IpBlockerRunnerRequirements.builder().build();
        }

        final String accessLog = buildAccessLog(args);
        final LocalDateTime startDate = buildStartDate(args);
        final Duration duration = buildDuration(args);
        final Long threshold = buildThreshold(args);

        return IpBlockerRunnerRequirements.builder()
                .accessLogFile(accessLog)
                .startDate(startDate)
                .threshold(threshold)
                .duration(duration)
                .build();
    }

    private static String buildAccessLog(ApplicationArguments args) {
        String accessLog = extractArgByName(args, "accessLog");
        if (accessLog == null)
            return null;

        return accessLog;
    }

    private static Duration buildDuration(ApplicationArguments args) {
        String duration = extractArgByName(args, "duration");
        if (duration == null)
            return null;

        try {
            return Duration.valueOf(duration.toUpperCase());
        } catch (Exception ex) {
            return null;
        }
    }

    private static Long buildThreshold(ApplicationArguments args) {
        String threshold = extractArgByName(args, "threshold");
        if (threshold == null)
            return null;

        return Long.valueOf(threshold);
    }

    private static LocalDateTime buildStartDate(ApplicationArguments args) {
        String startDate = extractArgByName(args, "startDate");
        if (startDate == null)
            return null;

        return LocalDateTime.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss"));
    }

    private static String extractArgByName(ApplicationArguments args, String argName) {
        List<String> optionValues = args.getOptionValues(argName);

        if (optionValues == null || optionValues.isEmpty()) {
            log.warning(String.format("Parameter --%s required.", argName));

            return null;
        }

        return optionValues.get(0);
    }
}
