package com.wh.ipblocker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ReaderSummary {

    private int validLinesCount;
    private int invalidLinesCount;
    private int bulkOperationsCount;

    private boolean failure;
    private String failureMessage;

    public void increaseValidLines() {
        validLinesCount++;
    }

    public void increaseInvalidLines() {
        invalidLinesCount++;
    }

    public void increaseBulkOperations() {
        bulkOperationsCount++;
    }

}
