package com.wh.ipblocker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * InternetProtocolStatusRepository represent an IP (Internet Protocol threshold) threshold for hourly or daily
 * measurements.
 *
 * @author 6massote
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class InternetProtocolThreshold {

    private String ip;
    private Long threshold;

}
