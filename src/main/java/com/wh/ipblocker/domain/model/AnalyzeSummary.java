package com.wh.ipblocker.domain.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class AnalyzeSummary {

    private List<InternetProtocolStatus> ips;

    public int getReachedLimitCount() {
        if (ips == null)
            return 0;

        return ips.size();
    }

}
