package com.wh.ipblocker.domain.model;

import liquibase.util.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class IpBlockerRunnerRequirements {

    private String accessLogFile;
    private LocalDateTime startDate;
    private Duration duration;
    private Long threshold;

    private Long jobId;

    public Path getAccessLogPath() {
        return Paths.get(accessLogFile);
    }

    public boolean isValid() {
        return StringUtils.isNotEmpty(accessLogFile)
                && startDate != null
                && duration != null
                && threshold != null;
    }

    public LocalDateTime getEndDate() {
        if (startDate == null)
            return null;

        LocalDateTime endDate = startDate.plusHours(1);

        if (Duration.DAILY.equals(duration)) {
            endDate = startDate.plusDays(1);
        }

        return endDate;
    }

}
