package com.wh.ipblocker.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "IP_STATUS")
@Builder
public class InternetProtocolStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "JOB")
    private Long job;

    @Size(min=1, max=32)
    @Column(nullable = false, name = "IP")
    private String ip;

    @Column(nullable = false, name = "THRESHOLD")
    private Long threshold;

    @Size(min=1, max=1024)
    @Column(nullable = false, name = "DESCRIPTION")
    private String description;

    @Column(nullable = false, name = "BLOCKED_AT")
    private LocalDateTime blockedAt;

    @Column(nullable = false, name = "BLOCKED_UNTIL_AT")
    private LocalDateTime blockedUntilAt;

}
