package com.wh.ipblocker.domain.model;

/**
 * Represent the duration time to decide if a specific Internet Protocol reach threshold limits or not.
 *
 * @author 6massote
 */
public enum Duration {
    HOURLY("hourly"), DAILY("daily");

    private final String duration;

    Duration(String duration) {
        this.duration = duration;
    }
}
