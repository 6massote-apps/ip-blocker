package com.wh.ipblocker.domain.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Builder
@Data
public class AnalyzerRequirements {

    private LocalDateTime startDate;
    private Duration duration;
    private Long threshold;

    public LocalDateTime getEndDate() {
        if (startDate == null)
            return null;

        LocalDateTime endDate = startDate.plusHours(1);

        if (Duration.DAILY.equals(duration)) {
            endDate = startDate.plusDays(1);
        }

        return endDate;
    }

    public boolean isValid() {
        return startDate != null
                && duration != null
                && threshold != null;
    }
}
