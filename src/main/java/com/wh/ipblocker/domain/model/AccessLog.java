package com.wh.ipblocker.domain.model;

import liquibase.util.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * AccessLog represents each one line of accesslog file.
 *
 * @author 6massote
 * @since 11/26/2017
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Table(name = "ACCESS_LOG")
@Entity
public class AccessLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "JOB")
    private Long job;

    @Column(nullable = false, name = "TIME")
    private LocalDateTime time;

    @Size(min=1, max=32)
    @Column(nullable = false, name = "IP")
    private String ip;

    @Size(min=1, max=1024)
    @Column(nullable = false, name = "DESCRIPTION")
    private String description;

    @Size(min=1, max=6)
    @Column(nullable = false, name = "STATUS_CODE")
    private String statusCode;

    @Size(min=1, max=512)
    @Column(nullable = false, name = "USER_AGENT")
    private String userAgent;

    public boolean isValid() {
        return time != null
                && StringUtils.isNotEmpty(ip)
                && StringUtils.isNotEmpty(description)
                && StringUtils.isNotEmpty(statusCode)
                && StringUtils.isNotEmpty(userAgent);
    }
}
