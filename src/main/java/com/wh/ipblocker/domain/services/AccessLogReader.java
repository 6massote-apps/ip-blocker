package com.wh.ipblocker.domain.services;

import com.wh.ipblocker.domain.model.AccessLog;
import com.wh.ipblocker.domain.model.IpBlockerRunnerRequirements;
import com.wh.ipblocker.domain.model.ReaderSummary;
import com.wh.ipblocker.storage.AccessLogRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@Log
@Service
public class AccessLogReader {

    @Value("${lines.bulk.operations}")
    public int LINES_BY_BULK_OPERATIONS;

    @Autowired
    private AccessLogRepository accessLogRepository;

    /**
     * Based on ipBlockerRunnerRequirements param, read each line and save to database in bulk operations.
     *
     * @param ipBlockerRunnerRequirements
     * @return if process works fine, return true. Otherwise, return false.
     */
    public ReaderSummary read(final IpBlockerRunnerRequirements ipBlockerRunnerRequirements) {
        final ReaderSummary readerSummary = new ReaderSummary();
        final List<AccessLog> accessLogBulkOperation = new ArrayList<>();

        if (!ipBlockerRunnerRequirements.isValid()) {
            log.warning(String.format("Aborting AccessLog reader. Some of parameters is required. Requirements=%s", ipBlockerRunnerRequirements));

            return ReaderSummary.builder()
                    .failure(true)
                    .build();
        }

        log.info(String.format("Starting accessLog reader for requirements=%s", ipBlockerRunnerRequirements));

        try (Stream<String> streamOflines = Files.newBufferedReader(ipBlockerRunnerRequirements.getAccessLogPath()).lines()) {

            streamOflines
                    .map(line -> {
                        final String[] lineSplit = line.split("\\|");

                        if (lineSplit.length != 5) {
                            log.warning(String.format("Invalid number of values for line=%s", Arrays.toString(lineSplit)));

                            return AccessLog.builder().build();
                        }

                        return AccessLog.builder()
                                .job(ipBlockerRunnerRequirements.getJobId())
                                .time(LocalDateTime.parse(
                                    lineSplit[0],
                                    DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")))
                                .ip(lineSplit[1])
                                .description(lineSplit[2])
                                .statusCode(lineSplit[3])
                                .userAgent(lineSplit[4])
                                .build();
                    })
                    .filter(accessLog -> {
                        if (accessLog.isValid()) {
                            readerSummary.increaseValidLines();
                            return true;
                        } else {
                            readerSummary.increaseInvalidLines();
                            log.warning(String.format("Invalid accessLog=%s format.", accessLog));
                            return false;
                        }
                    })
                    .forEach(accessLog -> {
                        accessLogBulkOperation.add(accessLog);

                        if (accessLogBulkOperation.size() >= LINES_BY_BULK_OPERATIONS) {
                            accessLogRepository.save(accessLogBulkOperation);

                            readerSummary.increaseBulkOperations();
                            accessLogBulkOperation.clear();
                        }
                    });
        } catch (IOException e) {
            log.severe(String.format("Error occurred during some file operation. Requirements=%s, Error: %s", ipBlockerRunnerRequirements, e.getLocalizedMessage()));

            return ReaderSummary.builder()
                    .failure(true)
                    .build();
        } catch (Exception e) {
            log.severe(String.format("General error occurred during accessLog processing. Requirements=%s, Error: %s", ipBlockerRunnerRequirements, e.getLocalizedMessage()));

            return ReaderSummary.builder()
                    .failure(true)
                    .build();
        }

        log.info(String.format("AccessLog processed. Summary=%s.", readerSummary));

        return readerSummary;
    }

}
