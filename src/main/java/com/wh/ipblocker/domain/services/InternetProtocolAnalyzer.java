package com.wh.ipblocker.domain.services;

import com.wh.ipblocker.domain.model.*;
import com.wh.ipblocker.storage.AccessLogRepository;
import com.wh.ipblocker.storage.InternetProtocolStatusRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class analyze all IPs processed form accessLog and save all IPs with threshold up
 * to required by requirements.
 *
 * @author 6massote
 */
@Log
@Service
public class InternetProtocolAnalyzer {

    public String HOURLY_LIMIT_EXCEEDED = "Your IP was blocked because of the %s limit - limit(%d) usage(%d)}";
    public String DAILY_LIMIT_EXCEEDED = "Your IP was blocked because of the %s limit - limit(%d) usage(%d)";

    @Autowired
    private AccessLogRepository accessLogRepository;

    @Autowired
    private InternetProtocolStatusRepository internetProtocolStatusRepository;

    /**
     * Analyze every Internet Protocol present in accessLog and save it to database with a block cause.
     *
     * @param analyzerRequirements startDate and endDate to check database for IPs to be blocked based on duration and threashold.
     */
    public AnalyzeSummary analyze(final IpBlockerRunnerRequirements analyzerRequirements) {

        if (!analyzerRequirements.isValid()) {
            log.warning(String.format("Aborting Analyzer. Some of parameters is required. Requirements=%s", analyzerRequirements));

            return AnalyzeSummary.builder().build();
        }

        // TODO - IMPROVEMENTS : refactor to use pagination instead of one request.
        final List<InternetProtocolThreshold> internetProtocolThresholds = internetProtocolStatusRepository.findIpsByThresholdAndByDate(
                        analyzerRequirements.getJobId(),
                        analyzerRequirements.getStartDate(),
                        analyzerRequirements.getEndDate(),
                        analyzerRequirements.getThreshold());

        final List<InternetProtocolStatus> ipsStatus = internetProtocolThresholds.stream()
                .filter(ipt -> {
                    return ipt.getThreshold() > analyzerRequirements.getThreshold();
                })
                .map(ipt -> {
                    return InternetProtocolStatus.builder()
                        .job(analyzerRequirements.getJobId())
                        .ip(ipt.getIp())
                        .threshold(ipt.getThreshold())
                        .description(buildThresholdMessage(
                                analyzerRequirements.getDuration(),
                                analyzerRequirements.getThreshold(),
                                ipt.getThreshold()))
                        .blockedAt(LocalDateTime.now(ZoneId.systemDefault()))
                        .blockedUntilAt(LocalDateTime.now(ZoneId.systemDefault()).plusDays(1))
                        .build();
                }).collect(Collectors.toList());

        // TODO - IMPROVEMENTS : may save using bulk operations instead single one. Return the count of ips saved.
        List<InternetProtocolStatus> ipsStatusSaved = internetProtocolStatusRepository.save(ipsStatus);

        return AnalyzeSummary.builder()
                .ips(ipsStatusSaved)
                .build();
    }

    private String buildThresholdMessage(final Duration duration, final Long thresholdLimit, final Long thresholdReached) {
        if (Duration.HOURLY.equals(duration)) {
            return String.format(HOURLY_LIMIT_EXCEEDED, duration, thresholdLimit, thresholdReached);
        }

        return String.format(DAILY_LIMIT_EXCEEDED, duration, thresholdLimit, thresholdReached);
    }

}
