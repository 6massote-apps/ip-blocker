package com.wh.ipblocker;

import com.wh.ipblocker.domain.model.AnalyzeSummary;
import com.wh.ipblocker.domain.model.AnalyzerRequirements;
import com.wh.ipblocker.domain.model.IpBlockerRunnerRequirements;
import com.wh.ipblocker.domain.model.ReaderSummary;
import com.wh.ipblocker.domain.services.InternetProtocolAnalyzer;
import com.wh.ipblocker.utils.validators.IpBlockerRunnerRequirementsHelper;
import com.wh.ipblocker.domain.services.AccessLogReader;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Log
@Component
@Order(1)
public class IpBlockerRunner implements ApplicationRunner {

    @Autowired
    private AccessLogReader accessLogReader;

    @Autowired
    private InternetProtocolAnalyzer internetProtocolAnalyzer;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        final IpBlockerRunnerRequirements ipBlockerRunnerRequirements = IpBlockerRunnerRequirementsHelper.convert(args);

        if (!ipBlockerRunnerRequirements.isValid()) {
            log.warning(String.format("Some of parameters is required. Requirements=%s", ipBlockerRunnerRequirements));
            return;
        }

        ipBlockerRunnerRequirements.setJobId(System.currentTimeMillis());
        log.info(String.format("Job Execution=%d", ipBlockerRunnerRequirements.getJobId()));

        ReaderSummary readerSummary = accessLogReader.read(ipBlockerRunnerRequirements);
        log.info(String.format("AccessLog processed. Summary=%s", readerSummary));

        if (readerSummary == null || readerSummary.isFailure()) {
            log.info(String.format("Aborting threshold analyzed. AccessLog processing failed. AccessLog summary=%s", readerSummary));
            return;
        }

        AnalyzeSummary analyzeSummary = internetProtocolAnalyzer.analyze(ipBlockerRunnerRequirements);
        log.info(String.format("IPs threshold analyzed. Summary=%s", analyzeSummary));

        if (analyzeSummary.getIps() != null)
            analyzeSummary.getIps().stream()
                    .forEach(ip -> System.out.println(String.format("Ip=%s, reason=%s", ip.getIp(), ip.getDescription())));

    }
}
