package com.wh.ipblocker.integration.storage;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.wh.ipblocker.domain.model.InternetProtocolThreshold;
import com.wh.ipblocker.storage.InternetProtocolStatusRepository;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class InternetProtocolStatusRepositoryTest {

    @Autowired
    private InternetProtocolStatusRepository repository;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("com.wh.ipblocker.fixtures");
    }

    @Test
    public void findIpsThresholdByDate() {
        final LocalDateTime startAt = LocalDateTime.of(2017, 1, 1, 0, 0, 0);
        final LocalDateTime endAt = LocalDateTime.of(2017, 1, 1, 0, 3, 0);

        final List<InternetProtocolThreshold> accessLogsDB = repository
                .findIpsByThresholdAndByDate(
                        1512053831708l,
                        startAt,
                        endAt,
                        1l);

        assertThat(accessLogsDB).hasAtLeastOneElementOfType(InternetProtocolThreshold.class);
    }

}