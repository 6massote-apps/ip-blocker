package com.wh.ipblocker.integration.storage;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.wh.ipblocker.domain.model.AccessLog;
import com.wh.ipblocker.storage.AccessLogRepository;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccessLogRepositoryTest {

    @Autowired
    private AccessLogRepository repository;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("com.wh.ipblocker.fixtures");
    }

    /**
     * This scenario is strict based on classpath:db/data/mock_data.csv
     */
    @Test
    public void context() {
        final List<AccessLog> accessLogs = repository.findAll();

        assertThat(accessLogs).isNotEmpty();
        assertThat(accessLogs).hasAtLeastOneElementOfType(AccessLog.class);
    }

    @Test
    public void save() {
        final AccessLog accessLog = Fixture.from(AccessLog.class).gimme("new");
        final AccessLog accessLogSaved = repository.save(accessLog);

        assertThat(accessLogSaved).isEqualTo(accessLog);
    }

}