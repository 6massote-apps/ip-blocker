package com.wh.ipblocker.unit.utils.validators;

import com.wh.ipblocker.domain.model.IpBlockerRunnerRequirements;
import com.wh.ipblocker.utils.validators.IpBlockerRunnerRequirementsHelper;
import org.junit.Test;
import org.springframework.boot.DefaultApplicationArguments;

import static org.assertj.core.api.Assertions.assertThat;

public class IpBlockerRunnerRequirementsHelperTest {

    @Test
    public void defaultApplicationArguments() {
        DefaultApplicationArguments defaultApplicationArguments = new DefaultApplicationArguments(new String[]{"--accessLog=/Users/gprado/Downloads/Java_MySQL_Test/access.log", "--startDate=2017-01-01.13:00:00", "--duration=hourly", "--threshold=100"});

        assertThat(defaultApplicationArguments.getOptionNames()).hasSize(4);
        assertThat(defaultApplicationArguments.getOptionNames()).contains("accessLog", "startDate", "duration", "threshold");
    }

    @Test
    public void validateAndConvert() throws Exception {
        DefaultApplicationArguments defaultApplicationArguments = new DefaultApplicationArguments(new String[]
                {"--accessLog=/Users/gprado/Downloads/Java_MySQL_Test/access.log",
                        "--startDate=2017-01-01.13:00:00",
                        "--duration=hourly",
                        "--threshold=100"});

        IpBlockerRunnerRequirements ipBlockerRunnerRequirements = IpBlockerRunnerRequirementsHelper.convert(defaultApplicationArguments);

        assertThat(ipBlockerRunnerRequirements).isNotNull();
        assertThat(ipBlockerRunnerRequirements.isValid()).isTrue();
        assertThat(ipBlockerRunnerRequirements.getAccessLogFile()).isNotNull();
        assertThat(ipBlockerRunnerRequirements.getAccessLogFile()).isEqualTo("/Users/gprado/Downloads/Java_MySQL_Test/access.log");
        assertThat(ipBlockerRunnerRequirements.getAccessLogPath()).isNotNull();
    }

    @Test
    public void givenMissedstartDate_validateAndConvert() throws Exception {
        DefaultApplicationArguments defaultApplicationArguments = new DefaultApplicationArguments(new String[]
                {"--accessLog=/Users/gprado/Downloads/Java_MySQL_Test/access.log",
                        "--duration=hourly",
                        "--threshold=100"});

        IpBlockerRunnerRequirements ipBlockerRunnerRequirements = IpBlockerRunnerRequirementsHelper.convert(defaultApplicationArguments);

        assertThat(ipBlockerRunnerRequirements.isValid()).isFalse();
    }

    @Test
    public void givenMissedDuration_validateAndConvert() throws Exception {
        DefaultApplicationArguments defaultApplicationArguments = new DefaultApplicationArguments(new String[]
                {"--accessLog=/Users/gprado/Downloads/Java_MySQL_Test/access.log",
                        "--startDate=2017-01-01.13:00:00",
                        "--threshold=100"});

        IpBlockerRunnerRequirements ipBlockerRunnerRequirements = IpBlockerRunnerRequirementsHelper.convert(defaultApplicationArguments);

        assertThat(ipBlockerRunnerRequirements.isValid()).isFalse();
    }

    @Test
    public void givenMissedThreshold_validateAndConvert() throws Exception {
        DefaultApplicationArguments defaultApplicationArguments = new DefaultApplicationArguments(new String[]
                {"--accessLog=/Users/gprado/Downloads/Java_MySQL_Test/access.log",
                        "--startDate=2017-01-01.13:00:00",
                        "--duration=hourly"});

        IpBlockerRunnerRequirements ipBlockerRunnerRequirements = IpBlockerRunnerRequirementsHelper.convert(defaultApplicationArguments);

        assertThat(ipBlockerRunnerRequirements.isValid()).isFalse();
    }

}