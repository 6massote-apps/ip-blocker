package com.wh.ipblocker.unit;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.wh.ipblocker.IpBlockerRunner;
import com.wh.ipblocker.domain.model.AnalyzeSummary;
import com.wh.ipblocker.domain.model.InternetProtocolStatus;
import com.wh.ipblocker.domain.model.IpBlockerRunnerRequirements;
import com.wh.ipblocker.domain.model.ReaderSummary;
import com.wh.ipblocker.domain.services.AccessLogReader;
import com.wh.ipblocker.domain.services.InternetProtocolAnalyzer;
import com.wh.ipblocker.fixtures.IpBlockerRunnerRequirementsFixture;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.DefaultApplicationArguments;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class IpBlockerRunnerTest {

    @Mock
    private InternetProtocolAnalyzer internetProtocolAnalyzer;

    @Mock
    private AccessLogReader accessLogReaderMock;

    @InjectMocks
    private IpBlockerRunner ipBlockerRunner;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("com.wh.ipblocker.fixtures");
    }

    @Test
    public void run() throws Exception {
        final DefaultApplicationArguments defaultApplicationArguments = Fixture.from(DefaultApplicationArguments.class).gimme("new");

        assertThat(defaultApplicationArguments.getOptionNames()).hasSize(4);
        assertThat(defaultApplicationArguments.getOptionNames()).contains("accessLog", "startDate", "duration", "threshold");

        ipBlockerRunner.run(defaultApplicationArguments);
    }

    @Test
    public void givenArgs_run() throws Exception {
        final DefaultApplicationArguments defaultApplicationArguments = Fixture.from(DefaultApplicationArguments.class).gimme("new");

        List<InternetProtocolStatus> status = Fixture
                .from(InternetProtocolStatus.class)
                .gimme(5, "new");

        when(accessLogReaderMock.read(any(IpBlockerRunnerRequirements.class)))
                .thenReturn(ReaderSummary.builder()
                .build());
        when(internetProtocolAnalyzer.analyze(any(IpBlockerRunnerRequirements.class)))
                .thenReturn(AnalyzeSummary.builder()
                        .ips(status)
                        .build());

        ipBlockerRunner.run(defaultApplicationArguments);

        verify(accessLogReaderMock).read(any(IpBlockerRunnerRequirements.class));
        verify(internetProtocolAnalyzer).analyze(any(IpBlockerRunnerRequirements.class));
    }

}