package com.wh.ipblocker.unit.domain.model;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.wh.ipblocker.domain.model.AccessLog;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;

public class AccessLogTest {

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("com.wh.ipblocker.fixtures");
    }

    @Test
    public void build() {
        final AccessLog accessLog = AccessLog.builder()
                .time(LocalDateTime.now(ZoneId.systemDefault()))
                .ip("192.168.1.1")
                .description("GET / HTTP/1.1")
                .statusCode("200")
                .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0")
                .build();

        assertThat(accessLog).isNotNull();
        assertThat(accessLog.getTime()).isNotNull();
        assertThat(accessLog.getIp()).isNotNull();
        assertThat(accessLog.getDescription()).isNotNull();
        assertThat(accessLog.getStatusCode()).isNotNull();
        assertThat(accessLog.getUserAgent()).isNotNull();
    }

    @Test
    public void fixture() {
        final AccessLog accessLog = Fixture.from(AccessLog.class).gimme("new");

        assertThat(accessLog).isNotNull();
        assertThat(accessLog.getTime()).isNotNull();
        assertThat(accessLog.getIp()).isNotNull();
        assertThat(accessLog.getDescription()).isNotNull();
        assertThat(accessLog.getStatusCode()).isNotNull();
        assertThat(accessLog.getUserAgent()).isNotNull();
    }

}