package com.wh.ipblocker.unit.domain.services;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.wh.ipblocker.domain.model.IpBlockerRunnerRequirements;
import com.wh.ipblocker.domain.model.ReaderSummary;
import com.wh.ipblocker.domain.services.AccessLogReader;
import com.wh.ipblocker.storage.AccessLogRepository;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class AccessLogReaderTest {

    @Mock
    private AccessLogRepository accessLogRepositoryMock;

    @InjectMocks
    private AccessLogReader accessLogReader;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("com.wh.ipblocker.fixtures");
    }

    @Test
    public void givenReaderRequirements_readWithSuccess() throws Exception {
        final IpBlockerRunnerRequirements ipBlockerRunnerRequirements = Fixture.from(IpBlockerRunnerRequirements.class).gimme("new");

        when(accessLogRepositoryMock.save(any(Iterable.class))).thenReturn(any(Iterable.class));

        ReaderSummary readerSummary = accessLogReader.read(ipBlockerRunnerRequirements);

        assertThat(readerSummary.isFailure()).isFalse();
        assertThat(readerSummary.getBulkOperationsCount()).isEqualTo(10);
        assertThat(readerSummary.getValidLinesCount()).isEqualTo(10);
        assertThat(readerSummary.getInvalidLinesCount()).isEqualTo(0);

        verify(accessLogRepositoryMock, times(10)).save(any(Iterable.class));
    }

    @Test
    public void givenReaderRequirements_readWithFileFailures() throws Exception {
        final IpBlockerRunnerRequirements ipBlockerRunnerRequirements = Fixture.from(IpBlockerRunnerRequirements.class).gimme("error");

        when(accessLogRepositoryMock.save(any(Iterable.class))).thenReturn(any(Iterable.class));

        ReaderSummary readerSummary = accessLogReader.read(ipBlockerRunnerRequirements);

        assertThat(readerSummary.isFailure()).isFalse();
        assertThat(readerSummary.getBulkOperationsCount()).isEqualTo(7);
        assertThat(readerSummary.getValidLinesCount()).isEqualTo(7);
        assertThat(readerSummary.getInvalidLinesCount()).isEqualTo(3);

        verify(accessLogRepositoryMock, times(7)).save(any(Iterable.class));
    }

    @Test
    public void givenReaderRequirementsEmpty_readWithFailure() throws Exception {
        ReaderSummary readerSummary = accessLogReader.read(IpBlockerRunnerRequirements.builder().build());

        assertThat(readerSummary.isFailure()).isTrue();
    }

}