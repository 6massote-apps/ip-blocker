package com.wh.ipblocker.unit.domain.services;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.wh.ipblocker.domain.model.*;
import com.wh.ipblocker.domain.services.InternetProtocolAnalyzer;
import com.wh.ipblocker.storage.InternetProtocolStatusRepository;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class InternetProtocolAnalyzerTest {

    @InjectMocks
    private InternetProtocolAnalyzer internetProtocolAnalyzer;

    @Mock
    private InternetProtocolStatusRepository internetProtocolStatusRepositoryMock;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("com.wh.ipblocker.fixtures");
    }

    @Test
    public void givenReaderRequirements_readWithSuccess() throws Exception {
        IpBlockerRunnerRequirements ipBlockerRunnerRequirements = Fixture
                .from(IpBlockerRunnerRequirements.class)
                .gimme("new");
        List<InternetProtocolThreshold> thresholds = Fixture
                .from(InternetProtocolThreshold.class)
                .gimme(5, "new");
        List<InternetProtocolStatus> status = Fixture
                .from(InternetProtocolStatus.class)
                .gimme(5, "new");

            when(internetProtocolStatusRepositoryMock
                    .findIpsByThresholdAndByDate(
                        ipBlockerRunnerRequirements.getJobId(),
                        ipBlockerRunnerRequirements.getStartDate(),
                        ipBlockerRunnerRequirements.getEndDate(),
                        ipBlockerRunnerRequirements.getThreshold()))
                    .thenReturn(thresholds);

        when(internetProtocolStatusRepositoryMock.save(any(List.class)))
                .thenReturn(status);

        AnalyzeSummary analyzeSummary = internetProtocolAnalyzer.analyze(ipBlockerRunnerRequirements);

        verify(internetProtocolStatusRepositoryMock, times(1))
                .findIpsByThresholdAndByDate(
                        any(Long.class),
                        any(LocalDateTime.class),
                        any(LocalDateTime.class),
                        any(Long.class));

        verify(internetProtocolStatusRepositoryMock, times(1))
                .save(any(Iterable.class));

        assertEquals(5, analyzeSummary.getReachedLimitCount());
    }

    @Test
    public void givenAnalyzeRequirementsEmpty_analyzeNothing() throws Exception {
        AnalyzeSummary analyzeSummary = internetProtocolAnalyzer.analyze(IpBlockerRunnerRequirements.builder().build());

        assertEquals(0, analyzeSummary.getReachedLimitCount());
    }

}