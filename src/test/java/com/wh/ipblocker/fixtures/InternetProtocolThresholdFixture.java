package com.wh.ipblocker.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.wh.ipblocker.domain.model.InternetProtocolThreshold;

public class InternetProtocolThresholdFixture implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(InternetProtocolThreshold.class).addTemplate("new", new Rule() {{
            add("ip", random("192.168.1.1", "192.168.2.2", "192.168.3.3", "192.168.4."));
            add("threshold", random(10l, 15l, 20l, 25l, 30l));
        }});
    }
}
