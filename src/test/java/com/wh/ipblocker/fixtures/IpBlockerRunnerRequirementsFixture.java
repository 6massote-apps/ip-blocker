package com.wh.ipblocker.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.wh.ipblocker.domain.model.Duration;
import com.wh.ipblocker.domain.model.IpBlockerRunnerRequirements;

import java.time.LocalDateTime;

public class IpBlockerRunnerRequirementsFixture implements TemplateLoader {

    @Override
    public void load() {

        Fixture.of(IpBlockerRunnerRequirements.class).addTemplate("new", new Rule() {{
            add("accessLogFile", ClassLoader.getSystemResource("accesslog-10lines.txt").getPath());
            add("startDate", LocalDateTime.of(2017, 1, 1, 0, 0, 10));
            add("duration", Duration.HOURLY);
            add("threshold", "2");
        }});

        Fixture.of(IpBlockerRunnerRequirements.class).addTemplate("error", new Rule() {{
            add("accessLogFile", ClassLoader.getSystemResource("accesslog-10lines-witherrors.txt").getPath());
            add("startDate", LocalDateTime.of(2017, 1, 1, 0, 0, 10));
            add("duration", Duration.HOURLY);
            add("threshold", "2");
        }});

    }

}
