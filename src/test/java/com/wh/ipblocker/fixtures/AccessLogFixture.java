package com.wh.ipblocker.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.wh.ipblocker.domain.model.AccessLog;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class AccessLogFixture implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(AccessLog.class).addTemplate("new", new Rule(){{
            add("job", System.currentTimeMillis());
            add("time", LocalDateTime.now(ZoneId.systemDefault()));
            add("ip", "192.168.1.1");
            add("description", "GET / HTTP/1.1");
            add("statusCode", "200");
            add("userAgent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0");
        }});
    }

}
