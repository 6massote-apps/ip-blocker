package com.wh.ipblocker.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import org.springframework.boot.DefaultApplicationArguments;

public class DefaultApplicationArgumentsFixture  implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(DefaultApplicationArguments.class).addTemplate("new", new Rule() {{
            add("args", new String[]
                    {"--accessLog=/Users/gprado/Downloads/Java_MySQL_Test/access.log",
                            "--startDate=2017-01-01.13:00:00",
                            "--duration=hourly",
                            "--threshold=100"});
        }});
    }
}
