package com.wh.ipblocker.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.wh.ipblocker.domain.model.InternetProtocolStatus;
import com.wh.ipblocker.domain.model.InternetProtocolThreshold;

import java.time.LocalDateTime;

/**
 * InternetProtocolStatus represents IPs status when it has blocked by application.
 *
 * @author 6massote
 * @since 11/29/2017
 */
public class InternetProtocolStatusFixture implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(InternetProtocolStatus.class).addTemplate("new", new Rule() {{
            add("job", System.currentTimeMillis());
            add("ip", random("192.168.1.1", "192.168.2.2", "192.168.3.3", "192.168.4.4"));
            add("threshold", random(5l, 10l, 15l, 20l));
            add("description", "Blocked at...");
            add("blockedAt", LocalDateTime.now());
            add("blockedUntilAt", LocalDateTime.now().plusDays(1));
        }});
    }
}
